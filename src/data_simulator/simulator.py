#!/usr/bin/python3
# -*- coding: utf-8; mode python -*-

"""
Functions to generate simulated display data
"""

import string
import random
from typing import Dict, List
from optparse import OptionParser
from datetime import datetime, timedelta
from database.models import Display, Ecommerce
from image_quality_manager import calculate_img_qa

from utils import generate_output
from logger import get_logger

from database.manager import (
    store_elasticsearch, drop_elasticsearch_index,
    delete_elasticsearch_index
)


PRICE_RULE_DATE = {
    "black_friday": {
        "start_date": datetime(2019, 10, 25),
        "offer_date": datetime(2019, 11, 29),
        "end_date": datetime(2019, 12, 5),
        "percent": 15,
        "offer_percent": 25,
    },
    "january_dates": {
        "start_date": datetime(2020, 1, 8),
        "offer_date": datetime(2020, 1, 10),
        "end_date": datetime(2020, 1, 31),
        "percent": 20,
        "offer_percent": 25
    }
}

SIMULATED_BRANDS = {
    "GondorElectronics": "GE",
    "LindonGlobals": "LinG",
    "TheShireTech": "ShireT",
    "GorgorothInnovations": "GorInn",
    "GundabadTechnologies": "GTech",
    "AngmarTechIn": "Angmar",
}
SIMULATED_SIZES = [18, 20, 22, 23, 24, 27, 28, 32]
SIMULATED_RESOLUTIONS = [
    "1366x768", "1280x800", "1600x900", "1920x1080", "1920x1200", "2048x1440",
    "2560x1440", "2560x1600", "3200x1800", "3840x2160", "5120x2160", "7680x4320",
]

SIMULATED_TIME_RESP_LEN = 7
SIMULATED_FREQ_RESP = [60, 70, 75, 80, 85, 90, 100, 110, 120, 160]


def get_sim_size() -> int:
    """
    Get a simulated size

    Returns:
        simulated size
    """
    pos = random.randint(0, len(SIMULATED_SIZES)-1)
    return SIMULATED_SIZES[pos]


def get_sim_resp_time() -> int:
    """
    Get a simulated response time

    Returns:
        simulated response time
    """
    return random.randint(1, SIMULATED_TIME_RESP_LEN)


def get_sim_freq() -> int:
    """
    Get a simulated frequency speed

    Returns:
        simulated refresh speed
    """
    pos = random.randint(0, len(SIMULATED_FREQ_RESP)-1)
    return SIMULATED_FREQ_RESP[pos]


def get_sim_resolution() -> [str, Dict]:
    """
    Get a simulated resolution

    Returns:
         simulated resolution
    """
    pos = random.randint(0, len(SIMULATED_RESOLUTIONS)-1)
    sim_res = SIMULATED_RESOLUTIONS[pos]
    res_values = sim_res.split("x")
    return sim_res, {'X': res_values[0], 'Y': res_values[1]}


def get_sim_model(brand: str) -> str:
    """
    Get a simulated model using the brand

    Args:
         brand: brand to gerate the model

    Returns:
        simulated model
    """
    length = random.randint(5+len(brand), 20) - len(brand)
    characters = string.ascii_letters + string.digits
    sim_model = brand
    sim_model += ''.join(random.sample(characters, length))
    return sim_model


def get_product_number() -> str:
    """
    Get a simulated resolution

    Returns:
         simulated product_number
    """
    length = random.randint(10, 10)
    characters = string.digits
    p_n = ''.join(random.sample(characters, length))
    return p_n


def get_sim_title(model: str, brand: str, size: str, resolution: str,
                  response_time: str, refresh_speed: str) -> str:
    """
    Get a simulated display title from given data as args

    Args:
        model: simulated model
        brand: simulated brand
        size: simulated size
        resolution: simulated resolution
        response_time: simulated response_time
        refresh_speed: simulated refresh_speed

    Returns:
        simulated title
    """

    return f"Display - {brand} {model}, {size}\", {resolution}, " \
           f"{response_time}s, {refresh_speed}Hz"


def get_sim_url(e_commerce, disp_title) -> str:
    """
    Get a simulated url of display data

    Args:
        e_commerce: ecommerce name
        disp_title: display simulated title

    Returns:
         the simulated url
    """
    title = disp_title.replace(' ', '_')
    title = title.replace(",", "")
    title = title.replace("-", "")
    title = title.replace("__", "_")
    title = title.replace("\"", "")
    return f"https://{e_commerce}.sim/{title}"


def get_sim_brand() -> str:
    """
    Get a simulated brand

    Returns:
         brand
    """
    keys = list(SIMULATED_BRANDS.keys())
    pos = random.randint(0, len(keys)-1)
    key = keys[pos]
    return key, SIMULATED_BRANDS.get(key)


def get_sim_init_price() -> float:
    """
    Get a simulated init price

    Returns:
         price
    """
    return round(random.uniform(100, 300), 2)


def get_sim_price(price: str, date: datetime) -> float:
    """
    Generate simulated price based on a date

    Args:
        price: initial price
        date: date

    Returns:
        price
    """
    for especial_day, dates_conf in PRICE_RULE_DATE.items():
        if (dates_conf.get("start_date") != dates_conf.get("offer_date")) \
            and dates_conf.get("start_date") < date < \
                dates_conf.get("offer_date"):
            percent = dates_conf.get("percent")
            rand_percent = random.uniform(percent - 2, percent + 2)
            return round(price + (price * rand_percent/100), 2)
        elif dates_conf.get("offer_date") < date < dates_conf.get("end_date"):
            percent = dates_conf.get("offer_percent")
            rand_percent = random.uniform(percent-2, percent+2)
            return round(price - (price * rand_percent/100), 2)
    return round(random.uniform(price-5, price+5), 2)


def gen_data(display: Display, num_days: int) -> List:
    """
    In function of a few days, propagate from the past until today to generate
    the price based on the dates

    Args:
        display: display data to propagate
        num_days: days to simulate

    Returns:
        list of generated data
    """
    price = display.price
    data = []
    now = datetime.now()
    start_date = now - timedelta(days=num_days)

    for day in range(1, num_days):
        date = start_date + timedelta(days=day)
        new_price = get_sim_price(price, date)
        new_disp = display.to_json()
        new_disp["price"] = new_price
        new_disp["date"] = date
        data.append(new_disp)
    return data


if '__main__' == __name__:

    parser = OptionParser(version="Script that generates simulated data")
    parser.add_option("-n", "--num-days", default=False, dest="num_days",
                      help="nom days to generate data")

    parser.add_option("-D", "--num-displays", dest="num_displays",
                      help="Generate data for x displays")

    parser.add_option("-c", "--crawler-name", dest="crawler_name",
                      help="Crawler name")

    parser.add_option("-d", "--database", dest="database",
                      help="database to store: sqlite, elasticsearch")

    parser.add_option("-C", "--collection", dest="collection",
                      help="collection or table to store in database")

    parser.add_option("--clean-db", dest="clean_db", action="store_true",
                      default=False, help="clean the database entries")

    parser.add_option("--delete-index", dest="delete_index",
                      action="store_true", default=False,
                      help="Delete previous data of the database")

    args, _ = parser.parse_args()

    generated_data = []

    crawler = args.crawler_name
    if not crawler:
        crawler = "simulated"

    if args.num_days and args.num_displays:
        logger = get_logger("simulator_data", crawler, 2)
        logger.debug("Start to generate simulated data for %s", crawler)

        e_commerce = Ecommerce(name=crawler)
        for idx_dis in range(0, int(args.num_displays)):
            sim_brand, sim_acronym = get_sim_brand()
            sim_model = get_sim_model(brand=sim_acronym)
            sim_prod_number = get_product_number()

            sim_size = get_sim_size()
            sim_resolution, sim_resolution_value = get_sim_resolution()
            image_quality = calculate_img_qa(sim_resolution)
            sim_response_time = get_sim_resp_time()
            sim_refresh_speed = get_sim_freq()

            sim_title = get_sim_title(
                sim_model, sim_brand, sim_size, sim_resolution,
                sim_response_time, sim_refresh_speed
            )
            sim_url = get_sim_url(crawler, sim_title)
            sim_price = get_sim_init_price()

            display = Display(
                title=sim_title,
                price=sim_price,
                brand=sim_brand,
                p_n=sim_prod_number,
                model=sim_model,
                size=sim_size,
                resolution=sim_resolution,
                resolution_values=sim_resolution_value,
                image_quality=image_quality,
                response_time=sim_response_time,
                refresh_speed=sim_refresh_speed,
                url=sim_url,
                e_commerce=e_commerce,
            )

            generated_data.extend(gen_data(display, int(args.num_days)))

        logger.debug("Generation simulated data for %s has been finished",
                     crawler)
        if args.database:
            if args.database == "elasticsearch" and args.collection:
                logger.debug("Storing data in Elasticsearch")
                store_elasticsearch(generated_data, args.collection)
                logger.debug("To store data in Elasticsearch has been finished")

        logger.debug("Generating output.")
        generate_output(
            data=generated_data,
            data_type="generated",
            crawler=f"{e_commerce.name}-simulator"
        )
        logger.debug("Simulator has been finished.")

    elif args.database and args.collection and args.clean_db:
        if args.database == "elasticsearch":
            drop_elasticsearch_index(es_index=args.collection)

    elif args.database == "elasticsearch" and args.collection \
            and args.delete_index:
        delete_elasticsearch_index(args.collection)
    else:
        parser.print_help()
