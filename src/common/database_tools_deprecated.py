
import os
import time
from pymongo import MongoClient
import constants as const

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from database.models import Base, Display
from elasticsearch import Elasticsearch


def get_connection_string(**kwargs):
    """Return a connection string for sqlalchemy::
        dialect+driver://username:password@host:port/database
    """
    if kwargs.get("db_type") == "mysql":
        connector_driver = "mysql+mysqldb"
    else:
        connector_driver = "sqlite"
        if not os.path.isdir(const.SQLITE_PATH):
            os.mkdir(const.SQLITE_PATH)

        sqlite_name = "%s_%s.db" % (
            time.strftime("%Y%m%d%Hh%Mm%Ss"),
            kwargs.get("client_db_name", os.environ.get("CLIENT_DB_NAME")),
        )
        return '%s:///%s' % (
            connector_driver,
            os.path.join(const.SQLITE_PATH, sqlite_name)
        )

    return '%s://%s:%s@%s:%s/%s' % (connector_driver,
        kwargs.get("client_db_username", os.environ.get("CLIENT_DB_USERNAME")),
        kwargs.get("client_db_password", os.environ.get("CLIENT_DB_PASSWORD")),
        kwargs.get("client_db_host", os.environ.get("CLIENT_DB_HOST")),
        kwargs.get("client_db_port", os.environ.get("CLIENT_DB_PORT")),
        kwargs.get("client_db_name", os.environ.get("CLIENT_DB_NAME"))
    )


def store_mongo(displays, collection):
    connection = MongoClient(
        host=const.MONGO_CLIENT_DB_HOST,
        port=const.MONGO_CLIENT_DB_PORT,
        username=const.MONGO_CLIENT_DB_USERNAME,
        password=const.MONGO_CLIENT_DB_PASSWORD,
        authSource=const.MONGO_CLIENT_DB_NAME
    )
    db = connection[const.MONGO_CLIENT_DB_NAME]
    collection = db[collection]

    for display in displays:
        collection.insert(display.document_model)

def store_elasticsearch(displays, collection):
    es = Elasticsearch(hosts=const.ELASTICSEARCH_HOSTS)

    for display in displays:
        if isinstance(display, Display):
            display = display.to_json()
        try:
            res = es.index(index=collection, body=display)
            #print(res['result'])
        except Exception as e:
            print ("display that raise the error: ", display)
            print(e)



def delete_index(index):
    es = Elasticsearch(hosts=const.ELASTICSEARCH_HOSTS)
    es.indices.delete(index=index, ignore=[400, 404])


def clean_elasticsearch(collection):
    es = Elasticsearch(hosts=const.ELASTICSEARCH_HOSTS)
    es.delete_by_query(index=collection, body={"query": {"match_all": {}}})


