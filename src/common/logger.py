#!/usr/bin/python3
# -*- coding: utf-8; mode python -*-

"""
Logger functions of Crawlers
"""

import os
import logging
import constants as c


def get_logger(script_name: str, crawler: str, version: int) -> logging.Logger:
    """
    Get logger of a given crawler

    Args:
        script_name: the script option to crawl
        crawler:  crawler name
        version: crawler version

    Returns:
        logger
    """

    logger_name = f"{crawler}_{c.LOGGER_DATE}_{script_name}.v{str(version)}.log"

    if not os.path.isdir(c.OUTPUT_LOGS_PATH):
        os.mkdir(c.OUTPUT_LOGS_PATH)

    output_log_dir = os.path.join(c.OUTPUT_LOGS_PATH, logger_name)

    logging.basicConfig(
        level=logging.INFO,
        format=c.LOGGER_FORMAT,
        filename=output_log_dir,
        filemode='w'
    )

    logger = logging.getLogger(logger_name)

    return logger
