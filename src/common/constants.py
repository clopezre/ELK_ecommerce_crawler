#!/usr/bin/python3
# -*- coding: utf-8; mode python -*-
"""
Common Constants and functions of Crawlers
"""

import os
import time
import logging


HOME = os.environ.get("HOME")

WORKING_DIRECTORY = os.getcwd()

DRIVER_PATH = os.environ.get(
    'CHROME_DRIVER', os.path.join(WORKING_DIRECTORY, '../chromedriver'))

OUTPUTS_PATH = os.environ.get(
    'OUTPUTS_PATH', os.path.join(WORKING_DIRECTORY, '../../outputs'))

BRANDS = [
    "ASUS",
    "AOC",
    "SAMSUNG",
    "LENOVO",
    "LG ELECTRONICS",
    "PHILIPS",
    "ACER",
    "HP",
    "IIYAMA",
    "BENQ",
    "DELL",
    "MSI",
    "ZOWIE"
]

IMAGE_QUALITY_CONF_FILE = os.path.join(
    WORKING_DIRECTORY, '../common/image_quality_conf.json')



# ============================= [database mongo] ===============================

MONGO_CLIENT_DB_USERNAME = os.environ.get(
    "MONGO_CLIENT_DB_USERNAME", "durin")
MONGO_CLIENT_DB_PASSWORD = os.environ.get(
    "MONGO_CLIENT_DB_PASSWORD", "Balrog2020!")
MONGO_CLIENT_DB_HOST = os.environ.get(
    "MONGO_CLIENT_DB_HOST", "127.0.0.1")
MONGO_CLIENT_DB_PORT = os.environ.get(
    "MONGO_CLIENT_DB_PORT", "3306")
MONGO_CLIENT_DB_NAME = os.environ.get(
    "MONGO_CLIENT_DB_NAME", "crawler_database")

# ============================== [elasticsearch] ===============================

ELASTICSEARCH_HOSTS = ['localhost:9200']


# =============================== [database SQL] ===============================

SQLITE_PATH = os.environ.get('SQLITE_PATH', os.path.join(OUTPUTS_PATH, 'db'))

POSTGRES_CONF = os.environ.get(
    "POSTGRES_CONF", os.path.join(
        WORKING_DIRECTORY, '../../config/postgres.conf'))

ELASTICSEARCH_CONF = os.environ.get(
    "ELASTICSEARCH_CONF", os.path.join(
        WORKING_DIRECTORY, '../../config/elasticsearch.conf'))


# ================================== [logger] ==================================

OUTPUT_LOGS_PATH = os.environ.get(
    'OUTPUT_LOGS_PATH', os.path.join(WORKING_DIRECTORY, '../../logs'))

LOGGER_DATE = time.strftime("%Y%m%d_%Hh%Mm%Ss")

LOGGER_LVLS = {
    "CRITICAL": logging.CRITICAL,
    "ERROR": logging.ERROR,
    "WARNING": logging.WARNING,
    "INFO": logging.INFO,
    "DEBUG": logging.DEBUG
}

LOGGER_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"


# =========================== [accuracy & results] =============================

OUTPUT_RESULTS_PATH = os.environ.get(
    'OUTPUT_LOGS_PATH', os.path.join(OUTPUTS_PATH, 'results'))

RESULTS_DATE = time.strftime("%Y%m%d_%Hh%Mm%Ss")

RESULTS_FILENAME = "%s_results.txt" % RESULTS_DATE
INCOMPLETE_FILENAME = "%s_incorrects.json" % RESULTS_DATE
XPATH_ERROR_FILENAME = "%s_xpath_errors.json" % RESULTS_DATE
REFRESH_ERROR_FILENAME = "%s_refresh_errors.json" % RESULTS_DATE

RESULTS_FILE_PATH = os.path.join(OUTPUT_RESULTS_PATH, RESULTS_FILENAME)
INCOMPLETE_FILE_PATH = os.path.join(OUTPUT_RESULTS_PATH, INCOMPLETE_FILENAME)
XPATH_ERROR_FILE_PATH = os.path.join(OUTPUT_RESULTS_PATH, XPATH_ERROR_FILENAME)
REFRESH_ERROR_FILE_PATH = os.path.join(OUTPUT_RESULTS_PATH,
                                       REFRESH_ERROR_FILENAME)