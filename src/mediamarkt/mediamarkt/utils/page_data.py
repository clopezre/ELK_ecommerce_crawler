#!/usr/bin/python3
# -*- coding: utf-8; mode python -*-
"""
Logic to parse data of MediaMarkt display URL
"""

import sys
import traceback
from logging import Logger
from typing import Union
from selenium import webdriver
import mediamarkt.utils.mediamarkt_constants as c
import constants as common
from scrapy.selector import Selector
from image_quality_manager import get_equivalence, get_resolution, \
    get_image_quality


def show_all(driver: webdriver):
    """
    Function to show all especifications list doing click over button

    Args:
        driver: selenium web driver
    """
    try:
        more_button = driver.find_element_by_xpath(
            c.XPATH_MORE_BUTTON
        )

        if more_button:
            more_button.click()
    except Exception as e:
        print(e)


def remove_size(size_unit_pos: int, model: str) -> str:
    """
    Remove display size from string to te model

    Args:
        size_unit_pos: display size pos
        model: display model unclean string

    Returns:
        model
    """
    pos = size_unit_pos - 4
    model = model.replace(",", ".")
    while True:
        try:
            float(model[pos:size_unit_pos])
            break
        except Exception as e:
            pos += 1

    model = model[:pos]
    return model


def clean_model(unclean_model: str, unclean_header_name: str, brand: str) -> str:
    """
    Clean the string that contains the display model, if it could not clean,
    get model from header name

    Args:
        unclean_model: unclean model string
        unclean_header_name: unclean model string
        brand: display brand

    Returns:
         model
    """
    if "Curvo MSI Optix MAG321CURV" in unclean_header_name:
        print()
    if not unclean_model or unclean_model not in unclean_header_name:
        brand_location = unclean_header_name.upper().find(brand) + \
                         len(brand)
        unclean_model = unclean_header_name[brand_location:]
        if ", " in unclean_model:
            model = unclean_model.split(", ")[0]
            if "\"" in model:
                size_unit_pos = model.find("\"")
                size_unit_pos = size_unit_pos - 1
                model = remove_size(size_unit_pos, model)
            if "\'\'" in model:
                size_unit_pos = model.find("\'\'")
                size_unit_pos = size_unit_pos-2
                model = remove_size(size_unit_pos, model)

            if model[0] == ' ':
                model = model[1:]

            return model

        unclean_model = unclean_model.split(" ")[0]
        return unclean_model.replace(",", "")
    else:
        return unclean_model


def clean_brand(unclean_brand: str, unclean_header_name: str) -> str:
    """
    Clean the string that contains the display brand, if it could not clean,
    get brand from header name

    Args:
        unclean_brand: unclean model string
        unclean_header_name: unclean model string

    Returns:
        brand
    """
    if unclean_brand:
        return unclean_brand
    else:
        for brand in common.BRANDS:
            if brand.upper() in unclean_header_name.upper():
                return brand


def clean_price(unclean_price: str) -> Union[float, None]:
    """
    Clean the string that contains the display price, if it could not clean,
    return Agotado

    Args:
        unclean_brand: unclean model string
        unclean_header_name: unclean model string

    Returns:
        price or None
    """
    if unclean_price:
        price = float(unclean_price)
    else:
        price = 'Agotado'
    return price


def clean_resolution(unclean_resolution: str, title: str,
                     qa_image: str, logger: Logger
                     ) -> [Union[float, None], Union[float, None]]:
    """
    Clean the string that contains the display resolution, if it could not clean,
    return None

    Args:
        qa_image: image quality of the display
        logger: logger object
        title: title of the item in web page
        unclean_resolution: string that contains the resolution

    Returns:
        the resolution in string and a dict with the X and Y axes.
    """
    if unclean_resolution:
        unclean_resolution = unclean_resolution.lower()
        unclean_resolution = unclean_resolution.replace("píxeles", "")
        resolution = unclean_resolution.replace(" x ", "x")
        resolution = resolution.split(" ")[0]
        if len(resolution) > 9:
            resolution = resolution[:9]
        resolution_values = resolution.split('x')
        resolution_values = {
            'X': float(resolution_values[0]),
            'Y': float(resolution_values[1])
        }
        qa_image = get_equivalence(qa_image)
    else:
        logger.debug(f"Get resolution from title [{title}] or "
                     f"from image quality [{qa_image}]")
        resolution = None
        resolution_values = None

        qa_eq = get_equivalence(qa_image)
        title_no_spaces = title.replace(" ", "")
        if not qa_image in title_no_spaces and not qa_eq in title_no_spaces:
            qa_image = get_image_quality(qa_eq)
            resolution, resolution_values = get_resolution(qa_image)
        else:
            if qa_eq in title:
                qa_image = qa_eq
            data = title_no_spaces.split(qa_image)
            if data:
                data = data[1]
                if data[0] == ",":
                    data = data[1:]

                data = data.split(',')
                if data:
                    data = data[0].replace(" ", "")
                    if "x" in data:
                        resolution = data
                        resolution_values = {
                            'X': float(resolution.split("x")[0]),
                            'Y': float(resolution.split("x")[1])
                        }
                    else:
                        qa_image = get_image_quality(qa_eq)
                        resolution, resolution_values = get_resolution(qa_image)

    return resolution, resolution_values, qa_image


def clean_frequency(unclean_frequency: str, title: str) -> Union[float, None]:
    """
    Clean the string that contains the display refresh speed,
    if it could not clean, return None

    Args:
        unclean_frequency: string that contains the refresh speed

    Returns:
        refresh speed or None
    """
    if unclean_frequency and not (unclean_frequency == 'N/A') \
            and ("-" != unclean_frequency):
        if "/" in unclean_frequency:
            unclean_frequency = unclean_frequency.split("/")[1]
        elif "-" in unclean_frequency:
            unclean_frequency = unclean_frequency.split("-")[1]
        refresh_speed = unclean_frequency.replace("Hz", "")
        refresh_speed = refresh_speed.replace("M", "")
        refresh_speed = refresh_speed.replace("A", "")
        refresh_speed = float(refresh_speed.replace("K", ""))
    else:
        ref_speed_pos = title.upper().find('HZ')
        if ref_speed_pos != -1:
            refresh_speed = title[ref_speed_pos - 3:ref_speed_pos]
            refresh_speed = refresh_speed.replace(" ", "")
        else:
            refresh_speed = None
    return refresh_speed


def clean_response_time(unclean_resp_time: str) -> Union[float, None]:
    """
    Clean the string that contains the display response time,
    if it could not clean, return None

    Args:
        unclean_resp_time: string that contains the response time

    Returns:
        response time or None
    """
    response_time = None
    if unclean_resp_time:
        unclean_resp_time = unclean_resp_time.replace("ms", "")
        unclean_resp_time = unclean_resp_time.replace(" ", "")
        try:
            response_time = float(unclean_resp_time)
        except ValueError:
            pass

    return response_time


def clean_size(unclean_size: str, unclean_header_name: str,
               model: str, logger: Logger) -> float:
    """
    Clean the string that contains the display size,
    if it could not clean, get from header name

    Args:
        unclean_size: unclean_resp_time: string that contains the size
        unclean_header_name: display header name
        model: display model
        logger: programm logger

    Returns:
        display size
    """
    if unclean_size:
        size = float(unclean_size.replace("\"", ""))
    else:
        model_location = unclean_header_name.find(model) + len(model) + 1
        unclean_size = unclean_header_name[model_location:]
        try:
            if '\"' in unclean_size:
                size = float(unclean_size.split("\"")[0])
            else:
                splitted = unclean_size.split("\'")[0]
                size = splitted.replace(" ", "")
                size = size.replace(",", ".")
                size = float(size)
        except Exception as e:
            logger.error(f"Error parsing header name: {unclean_header_name}")
            logger.error(e)
            sys.exit(f"Error parsing header name: {unclean_header_name}")
    return size


def clean_qa_image(unclean_qa_image: str):
    """
    Clean the image quality extracted from web page

    Args:
        unclean_qa_image: qa image to clean

    Returns:
        cleaned image quality
    """
    chars_to_delete = ["-", "ready"]

    for delete in chars_to_delete:
        unclean_qa_image = unclean_qa_image.replace(delete, "")

    return unclean_qa_image


def product_info(response: Selector, value: str) -> str:
    """
    Function to get data from html of display web page

    Args:
        response: scrapy response selector
        value: data to find

    Returns:
        data value
    """
    return response.xpath(c.XPATH_PRODUCT_INFO % value).extract_first()


def parse_display(response: Selector, url: str, logger: Logger):
    """
    Function to analyze the display web page and get data

    Args:
        response: scrapy response selector
        url: url to analyze and get data
        logger: Program logger

    Returns:
        extracted display data
    """
    unclean_header_name = response.xpath(c.XPATH_HEADER_NAME).extract_first()
    if unclean_header_name and 'REACONDICIONADO' in unclean_header_name:
        return None
    unclean_price = response.xpath(c.XPATH_PRICE_2).extract_first()
    p_n = response.xpath(c.XPATH_PRODUCT_NUMBER).extract_first()

    unclean_brand = product_info(response, "Fabricante:")
    unclean_model = product_info(response, "Modelo:")
    unclean_qa_image = product_info(response, "Calidad de imagen:")
    unclean_size = product_info(response, "Tamaño pantalla (pulgadas):")
    unclean_resolution = product_info(response, "Resolución:")
    unclean_resp_time = product_info(response, "Tiempo de respuesta:")
    unclean_frequency = product_info(response, "Frecuencia:")
    try:
        price = clean_price(unclean_price)
        response_time = clean_response_time(unclean_resp_time)
        refresh_speed = clean_frequency(unclean_frequency, unclean_header_name)

        brand = clean_brand(unclean_brand, unclean_header_name)
        model = clean_model(unclean_model, unclean_header_name, brand.upper())
        size = clean_size(unclean_size, unclean_header_name, model, logger)
        qa_image = clean_qa_image(unclean_qa_image)

        resolution, resolution_values, qa_image = clean_resolution(
            unclean_resolution, unclean_header_name, qa_image, logger
        )

        return {
            "name": unclean_header_name,
            "price": price,
            "brand": brand,
            "p_n": p_n,
            "model": model,
            "size": size,
            "image_quality": qa_image,
            "resolution": resolution,
            "resolution_values": resolution_values,
            "response_time": response_time,
            "refresh_speed": refresh_speed,
            "url": url
        }
    except Exception as e:
        logger.error(f"Error parsing data of {url}")
        logger.error(e)
        logger.error(traceback.format_exc())
