import contextlib
import inspect

from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool

from sqlalchemy_utils import database_exists, create_database

import os

__session_factory = None
__session = None
__engine = None
__is_test = False


def setup_db(*, is_test=False, **db_config):
    global __engine
    global __is_test

    if __engine:
        return

    __is_test = is_test

    connection_string = get_connection_string()
    connection_kwargs = db_config.get('connection_kwargs', {})

    # we always want to close connections
    connection_kwargs['poolclass'] = NullPool

    session_kwargs = db_config.get('session_kwargs', {})

    __engine = create_engine(connection_string, **connection_kwargs)

    if not database_exists(__engine.url):    # pragma: no cover
        print('Creating database: %s' % (__engine.url, ))
        create_database(__engine.url)

    create_tables()
    get_session(**session_kwargs)


def get_connection_string(**kwargs):
    """Return a connection string for sqlalchemy::
        dialect+driver://username:password@host:port/database
    """
    return 'mysql+mysqldb://%s:%s@%s:%s/%s' % (
        os.environ.get("CLIENT_DB_USERNAME"),
        os.environ.get("CLIENT_DB_PASSWORD"),
        os.environ.get("CLIENT_DB_HOST"),
        os.environ.get("CLIENT_DB_PORT"),
        os.environ.get("CLIENT_DB_NAME")
    )


def close_db():    # pragma: no cover
    if not __session:
        return
    try:
        __session.commit()
    except:
        __session.rollback()
    finally:
        __session.close()


def commit_session(_raise=True):    # pragma: no cover
    if not __session:
        return
    try:
        __session.commit()
    except Exception as e:
        __session.rollback()
        if _raise:
            raise


def _get_metadata():
    """
    Get all metadata
    """
    # We obtain all classes defined at models.py, but ModelMixin
    import database.models as db_models
    for name, obj in inspect.getmembers(db_models):
        if inspect.isclass(
                obj) and 'models.db_model_swaasbe.' in str(obj) and name is not 'ModelMixin':
            yield obj.metadata


def create_tables():
    """
    Create tables from metadata
    """
    assert __engine
    for meta in _get_metadata():
        meta.create_all(__engine)


def drop_tables(*, force=False):
    assert __engine
    for meta in _get_metadata():
        meta.drop_all(__engine)


def _clear_tables(*, force=False):
    if not __is_test and not force:
        return

    assert __engine

    meta = _get_metadata()
    with contextlib.closing(__engine.connect()) as con:
        trans = con.begin()
        for table in reversed(meta.sorted_tables):
            try:
                con.execute(table.delete())
            except:
                pass
        trans.commit()



def get_session(**kwargs):
    setup_db()

    assert __engine
    global __session
    global __session_factory

    if __session is not None:
        return __session

    if __session_factory is None:    # pragma: no cover
        __session_factory = sessionmaker(bind=__engine, **kwargs)

    __session = __session_factory()
    return __session


def session_getter(func):
    """Decorator to get a session and inject it as the first argument in a function"""

    def wrapper(*args, **kwargs):
        with dbtransaction() as session:
            return func(session, *args, **kwargs)

    return wrapper


def session_committer(func):

    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        finally:
            #log = get_logger()
            #log.info('session_committer')
            commit_session()

    return wrapper


@contextmanager
def dbtransaction():
    session = get_session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise