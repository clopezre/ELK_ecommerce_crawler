#!/usr/bin/python3
# -*- coding: utf-8; mode python -*-

"""
Logic to text transformation to get data
"""

import logging
from typing import Union, Dict


COMMON_REPLACES = ["?"]

TAGS = {
    "size": {
        "diagonal de la pantalla: ": {
            "split": " ",
            "position": 6,
            "replaces": ["(", ")", "\"", "\u201D"],
            "type": "float"
        },
        "tamaño (pulgada": {
            "split": " ",
            "position": 2,
            "replaces": ["\"", "\u201D", "s", ")"],
            "type": "float"
        },
        "tamaño de la pantalla": {
            "split": "pantalla",
            "position": 1,
            "replaces": [" ", "\xa0", "(", ")", "\"", "124.46", "60",
                         "pulgadas", "/", "68.5", "cm", "124.46", "62.2",
                         "\u201D"],
            "type": "float"
        },
        "pantalla (pulgadas)": {
            "split": " ",
            "position": 2,
            "replaces": ["\"", "\u201D"],
            "type": "float"
        },
        "tamaño pantalla": {
            "split": " ",
            "position": 2,
            "replaces": ["W", "\"", "\u201D"],
            "type": "float"
        },
        "dimensiones lcd (inch)": {
            "split": " ",
            "position": 3,
            "replaces": [],
            "type": "float"
        },
        "tamaño de pantalla (diagonal)": {
            "split": " ",
            "position": 6,
            "replaces": ["(", ")", "\"", "\u201D"],
            "type": "float"
        },
        "tamaño de pantalla (pulg.)": {
            "split": "tamaño de pantalla (pulg.)",
            "position": 1,
            "replaces": ["\"", "\u201D", " ", "(", "pulg.", ")", ":"],
            "type": "float"
        },
        "tamaño de pantalla": {
            "split": " ",
            "position": 3,
            "replaces": ["\"", "\u201D", " ", "(", "pulg.", ")", ":", "\xa0"],
            "type": "float"
        },
        "panel led curvo de grado": {
            "split": " ",
            "position": 0,
            "replaces": [" ", "\"", "A", "+", "\u201D"],
            "type": "float"
        },
        "pantalla de": {
            "split": " ",
            "position": 2,
            "replaces": ["”", "\u201D"],
            "type": "float"
        },
        "tamaño del panel": {
            "split": "panel",
            "position": 1,
            "replaces": [
                "\"", " ", "diagonal", "(", ")", "\u201D", "69cm", "pantalla",
                "ancha", '61.0cm', "68.47cm", "16:9", "6980cm", '60cm'
            ],
            "type": "float"
        },
        "pulgadas": {
            "split": " ",
            "position": 1,
            "replaces": ["\"", "\u201D"],
            "type": "float"
        },
        "diagonal (pulg.)": {
            "split": " ",
            "position": 2,
            "replaces": ["\"", "\u201D"],
            "type": "float"
        },
        "tamaño": {
            "split": " ",
            "position": 1,
            "replaces": ["\"", ":", "lcd", "", "\u201D"],
            "type": "float"
        },
        "tamaño lcd": {
            "split": "tamaño lcd",
            "position": 1,
            "replaces": [" ", "\"", "\u201D"],
            "type": "float"
        },
        "tamaño del lcd": {
            "split": "tamaño del lcd",
            "position": 1,
            "replaces": [" ", "\"", "\u201D", "(", ")", "pulgadas", "\t",
                         "16:9"],
            "type": "float"
        },
        "tamaño en diagonal": {
            "split": "diagonal",
            "position": 1,
            "replaces": ["\"", ":", "\u201D"],
            "type": "float"
        },
        "tamaño panel":  {
            "split": " ",
            "position": 2,
            "replaces": ["\"", "“", ":", "(", ")", "\u201D"],
            "type": "float"
        },
        "lcd tamaño":  {
            "split": "lcd tamaño",
            "position": 1,
            "replaces": ["\"", "“", ":", "(", ")", "\u201D"],
            "type": "float"
        },
        "tama?o": {
            "split": "monitor",
            "position": 1,
            "replaces": ["\"", "pulgadas", "\u201D"],
            "type": "float"
        },
        "tamaño en pulgadas": {
            "split": "pulgadas",
            "position": 1,
            "replaces": ["\"", "\u201D"],
            "type": "float"
        },
        "pantalla": {
            "split": "pantalla",
            "position": 1,
            "replaces": ["\"", "\u201D", ":"],
            "type": "float"
        },
        "tamańo monitor": {
            "split": "monitor",
            "position": 1,
            "replaces": ["\"", "\u201D", ":", "inch"],
            "type": "float"
        },
        "pantalla: ": {
            "split": "pantalla:",
            "position": 1,
            "replaces": [" ", "(", ")", "\"", "\u201D"],
            "type": "float"
        },
    },
    "resolution": {
        "resolución de la pantalla: ": {
            "split": "resolución de la pantalla: ",
            "position": 1,
            "replaces": [" ", "pixeles", "(WQHD)", "(", "max.", "máx.",
                         ")", "píxeles"],
            "type": "string",
            "max_length": 9,
            "min_length": 6,
        },
        "resolución de pantalla: ": {
            "split": "resolución de pantalla: ",
            "position": 1,
            "replaces": [" ", "pixeles", "(WQHD)", "(", "max.", ")",
                         "píxeles", "máx."],
            "type": "string",
            "max_length": 9,
            "min_length": 6,
        },
        "resolución": {
            "split": "resolución",
            "position": 1,
            "replaces": [
                " ", "px", "max.", "pixeles", ",", "(", "FullHD", ")", "máx.",
                "4K", "FHD", "píxeles", "bruta", "QHD", "a165Hz", "verdadera",
                "\xa0", ":", "75hz60hz-optimizadoparaconsola", "@", "75Hz"
            ],
            "especial_replace": ['*', "x"],
            "type": "string",
            "max_length": 9,
            "min_length": 6,
        },
        "resolcuión": {
            "split": "resolcuión",
            "position": 1,
            "replaces": [
                " ", "\xa0", "px", "max.", "máx.", "pixeles", "píxeles", ",",
                "(", "FullHD", ")", "4K", "FHD"
            ],
            "especial_replace": ['*', "x"],
            "type": "string",
            "max_length": 9,
            "min_length": 6,
        },
        "resolución:": {
            "split": "resolución:",
            "position": 1,
            "replaces": [
                " ", "px", "max.", "máx.", "pixeles", "píxeles", ",", "(",
                "FullHD", ")", "4K", "FHD",
            ],
            "especial_replace": ['*', "x"],
            "type": "string",
            "max_length": 9,
            "min_length": 6,
        },
        "resolución (máxima)": {
            "split": "resolución (máxima)",
            "position": 1,
            "replaces": [
                " ", "px", "max.", "máx.", "pixeles", "píxeles", ",", "(",
                "FullHD", ")", "4K", "FHD"
            ],
            "type": "string",
            "max_length": 9
        },
        "resolución máxima": {
            "split": "resolución máxima",
            "position": 1,
            "replaces": [
                " ", "px", "max.", "máx.", "pixeles", "píxeles", ",", "(",
                "FullHD", ")", "4K", "FHD", "at", "144hz", "hdmi", "2.0",
                ",", "dp", "@60Hz"
            ],
            "type": "string",
            "max_length": 9,
            "min_length": 6,
        },
        "resolución (max.) ": {
            "split": " a ",
            "position": 0,
            "replaces": ["resolución", "(", "max.", "máx.", ")", " "],
            "type": "string",
            "max_length": 9,
            "min_length": 6,
        },
        "resolución (máx.) ": {
            "split": " a ",
            "position": 0,
            "replaces": ["resolución", "(", "max.", "máx.", ")", " "],
            "type": "string",
            "max_length": 9,
        },
        "resolución máx.": {
            "split": "resolución máx.",
            "position": 1,
            "replaces": ["pixeles", " "],
            "type": "string",
            "max_length": 9,
            "min_length": 6,
        },
        "resolución nativa ": {
            "split": " a ",
            "position": 0,
            "replaces": ["resolución", "1080p", "nativa", "F", "HD", " ", "(",
                         ")", "max.", "pixeles", "ull", "hz", "a", "píxeles"],
            "type": "string",
            "max_length": 9,
            "min_length": 6,
        },
        "resolución del panel": {
            "split": "resolución del panel",
            "position": 1,
            "replaces": [" ", "(", ")", "\u00a0", "FHD", "WQHD", "max.",
                         "5K2K", "WUHD", "pixeles", "U", "píxeles"],
            "type": "string",
            "max_length": 9,
            "min_length": 6,
        },
        "hdmi:": {
            "split": "hdmi:",
            "position": 1,
            "replaces": [" ", "(", ")", "up", "to", "50hz"],
            "type": "string",
            "max_length": 9,
            "min_length": 6,
        },
        "resolución y frecuencia de actualización máx": {
            "split": "máx",
            "position": 1,
            "replaces": [" ", ".", "(", ")", "a", "144Hz"],
            "type": "string",
            "max_length": 9,
            "min_length": 6,
        },
    },
    "response_time": {
        "mpo de respuesta": {
            "split": "mpo de respuesta",
            "position": 1,
            "replaces": [
                " ", "ms", "(", ")", "Tipo", "GtG", "mprt", "VMPRT", "mayor",
                "velocidad", "MPRT", ":", "gris", "a", "\u00a0", "?", "t",
                "mas", "Más", "rápido", "rapido", "VRB", "ípico", "ipico",
                "1.", "Overdrive", "de", "panel"
            ],
            "type": "float",
        },
        "tiempo respuesta": {
            "split": "tiempo respuesta",
            "position": 1,
            "replaces": [" ", "ms", "(", ")"],
            "type": "float",
        },
    },
    "refresh_speed": {
        "máxima velocidad de actualización: ": {
            "split": "máxima velocidad de actualización: ",
            "position": 1,
            "replaces": [" ", "Hz"],
            "type": "float",
        },
        "refresco ": {
            "split": "refresco ",
            "position": 1,
            "replaces": [" ", "Hz"],
            "type": "float",
        },
        "refresco:": {
            "split": "refresco:",
            "position": 1,
            "replaces": [" ", "Hz"],
            "type": "float",
        },
        "frecuencia vert. ": {
            "split": "frecuencia vert. ",
            "position": 1,
            "replaces": [" ", "Hz", "(", ")"],
            "interval_values": ["~"],
            "type": "float",
        },
        "frecuencia vertical": {
            "split": "frecuencia vertical",
            "position": 1,
            "replaces": [" ", "Hz", "(", ")"],
            "interval_values": ["a", "~"],
            "type": "float",
        },
        "frecuencia de actualizaci\u00f3n": {
            "split": "frecuencia de actualizaci\u00f3n",
            "position": 1,
            "replaces": [" ", ":", "Hz", "(", "HDMI", ")",  "máx.", "estándar"],
            "interval_values": ["/"],
            "type": "float",
        },
        "hdmi frecuencia-v": {
            "split": "hdmi frecuencia-v",
            "position": 1,
            "replaces": [" ", "Hz"],
            "interval_values": ["~"],
            "type": "float",
        },
        "tasa de refresco:": {
            "split": "tasa de refresco:",
            "position": 1,
            "replaces": [" ", "Hz"],
            "type": "float",
        },
        "frecuencia": {
            "split": "frecuencia",
            "position": 1,
            "replaces": [" ", ":", "Hz", "\u00a0"],
            "type": "float",
        },
        "resolución nativa": {
            "split": "resolución nativa",
            "position": 1,
            "replaces": [" ", "Hz", "FHD", "DVI-DL", "DP", "1920", "1080",
                         "a", ",", "x", "(", ")"],
            "type": "float",
        },
        "resoluci\u00f3n (max.) ": {
            "split": "resoluci\u00f3n (max.) ",
            "position": 1,
            "replaces": [" ", "Hz", "FHD", "DVI-DL", "DP", "1920", "1080",
                         "a", "x", "(", ")"],
            "type": "float",
        },
        "vertical": {
            "split": "vertical",
            "position": 1,
            "replaces": [" ", "k", "Hz", ":", "(", ")", "max", "máx.",
                         "maxima", "máxima"],
            "interval_values": ["~", "-"],
            "type": "float",
        },
        "hdmi:": {
            "split": "/",
            "position": 1,
            "replaces": [" ", "k", "Hz", "v", "(", ")"],
            "interval_values": ["~"],
            "type": "float",
        },
        "frecuencia de barrido de pantalla (vertical)": {
            "split": "vertical",
            "position": 1,
            "replaces": [" ", "k", "Hz", "v", "(", ")", "hasta"],
            "interval_values": ["~"],
            "type": "float",
        },
        "resolución y frecuencia de actualización máx": {
            "split": "resolución y frecuencia de actualización máx",
            "position": 1,
            "replaces": [" ", ".1920x1080a", "Hz"],
            "type": "float",
        },
        'max. resolución': {
            "split": "(",
            "position": 1,
            "replaces": [" ", "hz", "-", "optimizado", "para", "consola", ")"],
            "interval_values": [","],
            "type": "float",
        },
        "resolución máxima": {
            "split": "at",
            "position": 1,
            "replaces": [" ", "hz", "(", ")", "hdmi", "2.0", ",", "dp"],
            "type": "float",
        },
        "resolución": {
            "split": "@",
            "position": 1,
            "replaces": [" ", "hz"],
            "type": "float",
        },
        "ecuencia": {
            "split": "ecuencia",
            "position": 1,
            "replaces": [" ", "hz", ":"],
            "type": "float",
        },
    }
}


def get_data(data: str, to_find: str, logger: logging.Logger,
             name: str = None) -> Union[float, str, Dict, None]:
    """
    Analyze the specification text data to extract the value that is in to_find

    Args:
        data: text to analyze
        to_find: specification to find and parse
        logger: logger of program
        name: name/title of display

    Returns:
        the data
    """
    if "cion" in data:
        data = data.replace("cion", "ción")

    tag_list = TAGS.get(to_find)
    if to_find == "refresh" and "horiz" in data:
        return None

    for key, conf in tag_list.items():
        if key in data:
            value = None
            splitted = data.split(conf.get("split"))
            try:
                value = splitted[conf.get("position")]

                for repl in COMMON_REPLACES:
                    value = value.replace(repl.lower(), "")

                for repl in conf.get("replaces"):
                    value = value.replace(repl.lower(), "")

                especial_replace = conf.get("especial_replace")
                if especial_replace:
                    value = value.replace(especial_replace[0],
                                          especial_replace[1])

                interval_values = conf.get("interval_values")
                if interval_values:
                    for separator in interval_values:
                        if separator in value:
                            values = value.split(separator)
                            values = list(map(lambda value: float(value),
                                             values))
                            values.sort()
                            value = values[-1]

                if conf.get("type") == "float":

                    if to_find == "response_time":
                        if ',' in value:
                            values = value.split(',')
                            values = list(map(lambda value: float(value),
                                              values))
                            values.sort()
                            value = values[0]

                    if isinstance(value, str) and "," in value:
                        value = value.replace(",", ".")
                    try:
                        if isinstance(value, str) and len(value) > 5:
                            continue
                        value = float(value)
                    except ValueError:
                        continue
                if to_find == "resolution":
                    if conf.get("type") == "string":
                        if len(value) > conf.get("max_length") \
                                or len(value) < conf.get("min_length"):
                            continue
                return value

            except ValueError as value_error:
                logger.error(f"Finding {to_find} - Error parsing [{data}] of "
                             f"[{name}] with key={key} => "
                             f"{data} => {value} >> VALUE_ERROR: {value_error}")

            except IndexError as index_error:
                logger.error(f"Finding {to_find} - Error parsing [{data}] of "
                             f"[{name}] with key={key} => "
                             f"{data} => {splitted}[{conf.get('position')}] >> "
                             f"INDEX_ERROR: {index_error}")

            except Exception as generic_error:
                logger.error(f"Uncatch error parsing{to_find} of [{name}]: "
                             f"{data} => GENERIC_ERROR: {generic_error}")

    return None
