# -*- coding: utf-8 -*-
import os
import scrapy


class DisplaySpider(scrapy.Spider):
    name = 'display'
    allowed_domains = ['www.pccomponentes.com/monitores-pc']
    start_urls = ['https://www.pccomponentes.com/monitores-pc/']
    url_pccomponentes = 'https://www.pccomponentes.com'

    def parse(self, response):
        names = response.xpath(
            '//*[@id="articleListContent"]/div/div/article/@data-name'
        ).extract()
        prices = response.xpath(
            '//*[@id="articleListContent"]/div/div/article/@data-price'
        ).extract()
        brands = response.xpath(
            '//*[@id="articleListContent"]/div/div/article/@data-brand'
        ).extract()
        relative_urls = response.xpath(
            '//*[@id="articleListContent"]/div/div/article/div[1]/a/@href'
        ).extract()

        items = len(names)

        for item in range(0, items):
            name = names[item]
            price = prices[item]
            brand = brands[item]
            relative_url = relative_urls[item]
            item_url = os.path.join(self.url_pccomponentes,relative_url)

            yield{
                'name': name,
                'price': price,
                'brand': brand,
                'item_url': item_url
            }






