# -*- coding: utf-8 -*-
import os
from time import sleep
from scrapy import Spider
from scrapy.selector import Selector
from scrapy.http import Request
from selenium import webdriver
from datetime import datetime

DRIVER_PATH = '/crawlers/chromedriver_80'
PCCOMPONENTES_ROOT_PAGE = 'https://www.pccomponentes.com'
PCCOMPONENTES_DISPLAY_PAGE = 'monitores-pc/'

XPATH_URLS = '//*[@id="articleListContent"]/div/div/article/div[1]/a/@href'
XPATH_NUM_ARTICLES = '//*[@id="totalArticles"]/text()'
XPATH_DISPLAY_IDS = '//*[@id="articleListContent"]/div/div/article/@data-id'


XPATH_DISPLAY_NAME = '//*[@id="contenedor-principal"]/div[2]/div/div[3]' \
                     '/div/div/div[1]/h1/strong/text()'
XPATH_DISPLAY_PRICE = '//*[@id="precio-main"]/@data-price'
# XPATH_DISPLAY_BRAND = '//*[@id="contenedor-principal"]/div[2]/div/div[4]/div[1]/div[2]/div[2]/a/text()'

XPATH_DISPLAY_BRAND = '//*[@id="contenedor-principal"]/div/div/div/div/div/div/a/text()'
#XPATH_DISPLAY_PN ='//*[@id="contenedor-principal"]/div[2]/div/div[4]/div[1]/div[2]/div[2]/span[1]/text()'
XPATH_DISPLAY_PN = '//*[@id="contenedor-principal"]/div/div/div/div/div/div' \
                   '/span/text()'


#XPATH_DISPLAY_SHIPPING = '//*[@id="contenedor-principal"]/div[2]/div/div[4]/div[1]/div[3]/div[2]/text()'

XPATH_DISPLAY_SHIPPING = '//*[@id="contenedor-principal"]/div[2]/div/div[4]/div/div[3]/div[2]/text()'

class PccomponenetesSeleniumSpider(Spider):

    name = 'pccomponenetes_selenium'
    allowed_domains = ['www.pccomponentes.com/monitores-pc/']

    def close_iframe(self):
        try:
            self.driver.switch_to.frame(self.driver.find_element_by_xpath(
                '//*[@class="cn_modal_iframe"]')
            )
            self.driver.find_element_by_xpath('/html/body/div/a').click()
        except:
            pass

    def parse_display(self, response):
        name = response.xpath(XPATH_DISPLAY_NAME).extract_first()
        price = float(response.xpath(XPATH_DISPLAY_PRICE).extract_first())
        brand = response.xpath(XPATH_DISPLAY_BRAND).extract_first()
        p_n = response.xpath(XPATH_DISPLAY_PN).extract_first()
        shipping = response.xpath(XPATH_DISPLAY_SHIPPING).extract_first()
        if shipping:
            shipping = shipping.replace(
                '\n', ''
            )

        yield{
            'name': name,
            'price': price,
            'brand': brand,
            'p_n': p_n,
            'shipping': shipping,
            'url': response.url,
            'date': datetime.today()
        }

    def start_requests(self):
        self.driver = webdriver.Chrome(DRIVER_PATH)

        self.driver.get(
            os.path.join(PCCOMPONENTES_ROOT_PAGE, PCCOMPONENTES_DISPLAY_PAGE
                         )
        )
        sleep(1)
        cookies_buton = self.driver.find_element_by_xpath(
            # '//*[@id="familia-secundaria"]/div[5]/div/div/div[2]/button'
            '//*[text()="ACEPTAR"]'
        )

        if cookies_buton:
            cookies_buton.click()

        self.close_iframe()

        sel = Selector(text=self.driver.page_source)
        num_displays = int(
            sel.xpath(XPATH_NUM_ARTICLES).extract_first())
        displays_ids = []
        previous_id = 0

        self.driver.find_element_by_xpath(
            '//*[@id="btnMore"]'
        ).click()

        while len(displays_ids) < num_displays:
            sel = Selector(text=self.driver.page_source)
            displays_ids = sel.xpath(XPATH_DISPLAY_IDS).extract()
            last_id = displays_ids[-1]

            if previous_id == last_id:
                self.driver.execute_script("window.scrollTo(0, %s);" % str(
                    location.get('y') + 100))

            element = self.driver.find_element_by_xpath(
                '//*[@data-id="%s"]' % last_id)
            self.driver.execute_script("arguments[0].scrollIntoView(false);",
                                  element)
            location = element.location
            previous_id = last_id

        sel = Selector(text=self.driver.page_source)
        relative_urls = sel.xpath(
            '//*[@id="articleListContent"]/div/div/article/div[1]/a/@href'
        ).extract()

        for display in relative_urls:
            url = os.path.join(
                PCCOMPONENTES_ROOT_PAGE,
                display.replace('/', '')
            )
            yield Request(url, callback=self.parse_display)
